
package revision.springboot.restapi.SpringBootRestApiRevision;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class SpringBootRestApiRevisionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestApiRevisionApplication.class, args);
	}

}
