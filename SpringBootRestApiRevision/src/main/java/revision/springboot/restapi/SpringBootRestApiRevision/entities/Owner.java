
/*
 * This class is a template of an owner object.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "owners")
@SuppressWarnings("unused")
public class Owner {

    // Fields.
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String middleName;

    @Column(nullable = false)
    private String lastName;

    @ManyToOne(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            targetEntity = Vehicle.class
    )
    @JsonBackReference
    @JoinColumn(name = "fk_vehicle")
    private Vehicle vehicle;

    // Default Constructor.
    public Owner() {}

    // Parameterized Constructor.
    public Owner(String firstName, String middleName, String lastName) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    // Getters and Setters for every field.
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    // ".equals()" method implementation to compare two "Owner" objects.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Owner owner)) return false;
        return getId() == owner.getId() && Objects.equals(getFirstName(), owner.getFirstName()) && Objects.equals(getMiddleName(), owner.getMiddleName()) && Objects.equals(getLastName(), owner.getLastName()) && Objects.equals(getVehicle(), owner.getVehicle());
    }

    // "hashCode()" method implementation.
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getMiddleName(), getLastName(), vehicle);
    }
}
