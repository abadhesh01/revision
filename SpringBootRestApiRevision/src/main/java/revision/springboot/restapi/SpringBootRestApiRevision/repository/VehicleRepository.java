
/*
 * This interface provides the implementation of "DAO(Data Access Object) / Repository" layer.
 * This performs all kinds of database operations like CREATE, UPDATE, DELETE AND SELECT etc.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.Vehicle;

@Repository // This repository is responsible for CRUD operations on "Vehicle" objects.
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    @Query("SELECT v FROM Vehicle v WHERE v.serialNumber = (SELECT MAX(v2.serialNumber) FROM Vehicle v2)")
    Vehicle findLastVehicle();

    @Query("SELECT v FROM Vehicle v JOIN FETCH v.owners WHERE v.serialNumber = (SELECT MAX(v2.serialNumber) FROM Vehicle v2)")
    Vehicle findLastVehicleWithOwners();

}
