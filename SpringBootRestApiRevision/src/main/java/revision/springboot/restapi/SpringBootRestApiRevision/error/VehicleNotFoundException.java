
/*
 * This is a type of "RuntimeException".
 * This class object is sent as a response with a message to the client if the vehicle with the provided
 * "serial number" was not found.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class VehicleNotFoundException extends RuntimeException {

    public VehicleNotFoundException(long serialNumber) {
        super("Vehicle with serial number '" + serialNumber + "' was not found!");
    }

}
