
/*
 * This class is the implementation of "ServiceProvider" interface.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.service;

import jakarta.persistence.LockModeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.Owner;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.Vehicle;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.VehicleServiceRecord;
import revision.springboot.restapi.SpringBootRestApiRevision.error.VehicleNotFoundException;
import revision.springboot.restapi.SpringBootRestApiRevision.repository.VehicleRepository;
import revision.springboot.restapi.SpringBootRestApiRevision.repository.VehicleServiceRecordRepository;
import revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage.ServerResponse;
import revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage.ServerResponseWithDetails;

import java.util.*;
import java.util.stream.Collectors;

@Service
@SuppressWarnings("unused")
public class ServiceProviderImpl implements ServiceProvider {

    // Getting the DAO (/ repository layer object
    // for CRUD operations on objects of "Vehicle" type).
    @Autowired
    private VehicleRepository vehicleRepository;

    // Getting the DAO (/ repository layer object
    // for CRUD operations on objects of "VehicleServiceRecord" type).
    @Autowired
    private VehicleServiceRecordRepository serviceRecordRepository;

    private final Logger LOGGER = LoggerFactory.getLogger(ServiceProviderImpl.class);

    @Transactional(isolation = Isolation.SERIALIZABLE, readOnly = true)
    @Override
    public List<Vehicle> getAllVehicles() {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        return vehicleRepository.findAll();
    }

    @Transactional(isolation = Isolation.SERIALIZABLE, readOnly = true)
    @Override
    public Object getVehicle(long serialNumber) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        return vehicleRepository.findById(serialNumber)
                .orElseThrow(() -> new VehicleNotFoundException(serialNumber));
    }

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    @Override
    public ServerResponseWithDetails addVehicle(Vehicle vehicle, boolean addDummyVehicleServiceRecords) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        // Calling the method "getDummyServiceRecords()" first to check TRANSACTIONS.
        // This has nothing got to do with adding the provided vehicle to the database.
        List<VehicleServiceRecord> dummyServiceRecords = getDummyServiceRecords(vehicle.getRegistrationNumber(), addDummyVehicleServiceRecords);

        // For each owner of the vehicle to be added to the database, setting their vehicle reference
        // to the vehicle to be added to the database.
        vehicle.getOwners().forEach((Owner currentOwner) -> currentOwner.setVehicle(vehicle));

        // Saving the vehicle along with its list of owners to the database and sending back the response.
        Vehicle newVehicle = vehicleRepository.save(vehicle);
        return new ServerResponseWithDetails(
                new ServerResponse("The vehicle has been added successfully.", newVehicle),
                dummyServiceRecords);
    }

    @Lock(LockModeType.OPTIMISTIC)
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    @Override
    @SuppressWarnings("all")
    public ServerResponse updateVehicle(Vehicle vehicle) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        // Retrieving the vehicle for the given serial number from the database and
        // if the vehicle with the given serial number is not present in the database,
        // then generating and throwing "VehicleNotFoundException", otherwise proceeding
        // next.
        Vehicle existingVehicle =
        vehicleRepository.findById(vehicle.getSerialNumber())
                .orElseThrow(() -> new VehicleNotFoundException(vehicle.getSerialNumber()));

        // Updating all fields of the vehicle.
        existingVehicle.setRegistrationNumber(vehicle.getRegistrationNumber());
        existingVehicle.setBrand(vehicle.getBrand());
        existingVehicle.setModel(vehicle.getModel());
        existingVehicle.setType(vehicle.getType());
        existingVehicle.setVariant(vehicle.getVariant());

        // Iterating through the owners list to be added / updated for the retrived
        // vehicle only if the list of the owners is not empty.
        if(vehicle.getOwners() != null && !vehicle.getOwners().isEmpty()) {
            vehicle.getOwners().forEach((Owner currentOwner) -> {
                // If the ID of the owner is '0'; then the owner is a new owner and
                // adding him / her to the list of owners for the retrived vehicle.
                if(currentOwner.getId() == 0) {
                    // Adding the new owner to the the list of the owners from the retrived vehicle
                    // and also setting the vehicle reference of new owners to the retrived vehicle.
                    currentOwner.setVehicle(existingVehicle);
                    existingVehicle.getOwners().add(currentOwner);
                }
                // Otherwise
                else
                {
                    // Checking for the ID value (other than '0') of the owner exists in the list of owners
                    // from the retrived vehicle.
                    // If the owners exist, updating the value of all of it's fields.
                    List<Owner> existingOwner = existingVehicle.getOwners()
                            .stream()
                            .filter((Owner owner) -> currentOwner.getId() == owner.getId())
                            .collect(Collectors.toList());
                    if (!existingOwner.isEmpty()) {
                        existingOwner.get(0).setFirstName(currentOwner.getFirstName());
                        existingOwner.get(0).setMiddleName(currentOwner.getMiddleName());
                        existingOwner.get(0).setLastName(currentOwner.getLastName());
                    }
                }
            });
        }
        // Saving / Updating the the vehicle along with it's list of owner to the database.
        vehicleRepository.save(existingVehicle);

        // Returning the response.
        return new ServerResponse("The vehicle has been updated successfully.",
                vehicleRepository.findById(vehicle.getSerialNumber()).get());
    }

    @Lock(LockModeType.OPTIMISTIC)
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE)
    @Override
    public ServerResponse deleteVehicle(long serialNumber) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        // Retrieving the vehicle for the given serial number from the database and
        // if the vehicle with the given serial number is not present in the database,
        // then generating and throwing "VehicleNotFoundException", otherwise deleting the
        // vehicle from the database along with its list of owners.
        vehicleRepository.findById(serialNumber)
                .orElseThrow(() -> new VehicleNotFoundException(serialNumber));
        vehicleRepository.deleteById(serialNumber);
        // Returning the response.
        return new ServerResponse("The vehicle with serial number '" + serialNumber
                + "' has been deleted successfully.", null);
    }

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Transactional(propagation = Propagation.MANDATORY, isolation = Isolation.SERIALIZABLE)
    public List<VehicleServiceRecord> getDummyServiceRecords(String vehicleRegistrationNumber,
                                                        boolean enableAddDummyVehiclesToServiceRecords) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        /* ********************************************************************
           This function adds dummy service records with dummy dates and
           provided registration number of a vehicle to the database.
           ******************************************************************** */

        // Parameter "enableAddDummyVehiclesToServiceRecords" specifies whether to add dummy service records
        // to the database or not. If not, then the method statements are not further executed.
        if(!enableAddDummyVehiclesToServiceRecords) {
            return serviceRecordRepository.findAll();
        }

        // Creating an instance of Random class to generate random numbers.
        Random random = new Random();

        // Creating a list to store dummy vehicle service records.
        List<VehicleServiceRecord> dummyVehicleServiceRecords = new LinkedList<>();

        // Specifying the number of records to be added in the list between 5 and 10 (inclusive).
        // It is randomly specified by the process.
        int numberOfRecords = random.nextInt(6) + 5;

        // Adding dummy vehicle services record to the list.
        while (numberOfRecords > 0) {

            // Generating a random year between 2000 and 2022 (inclusive).
            int year = random.nextInt(23) + 2000; // 23 years from 2000 to 2022

            // Generating a random month between 0 (January) and 11 (December).
            int month = random.nextInt(12);

            // Generating a random day between 1 and 31 (you may want to adjust this based on the month).
            int day = random.nextInt(31) + 1;

            // Creating a random Date.
            @SuppressWarnings("all")
            Date randomDate = new Date(year - 1900, month, day);

            // Creating a vehicle service record with random date and provided registration number and adding it
            // to the list.
            dummyVehicleServiceRecords.add(new VehicleServiceRecord(randomDate, vehicleRegistrationNumber));

            // Decrementing the number of records to be added by 1.
            numberOfRecords --;
        }

       // Adding the list of vehicle service records to the database.
       serviceRecordRepository.saveAll(dummyVehicleServiceRecords);

       // EXTRACTING and RETURNING all the service records.
       return serviceRecordRepository.findAll();
    }
}
