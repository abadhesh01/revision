
/*
 * This class is the "exception handler" that, handles all the
 * exceptions / errors and sends error response back to the client.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import revision.springboot.restapi.SpringBootRestApiRevision.response.errorMessage.ErrorResponse;

import java.util.Date;

@ControllerAdvice
@SuppressWarnings("unused")
public class AllExceptionsHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(AllExceptionsHandler.class);

    // Handling "VehicleNotFoundException" if the vehicle with provided serial number was not found for any request.
    @ExceptionHandler(VehicleNotFoundException.class)
    public ResponseEntity<?> handleVehicleNotFoundException(VehicleNotFoundException exception, WebRequest request) {
        ErrorResponse response = new ErrorResponse(
                exception.getClass().getName(),
                exception.getMessage(),
                new Date());

        LOGGER.info(String.valueOf(response));

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    // Handling all the exceptions except "VehicleNotFoundException" that might occur.
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleAnyException(Exception exception, WebRequest request) {
        ErrorResponse response = new ErrorResponse(
                exception.getClass().getName(),
                exception.getMessage(),
                new Date());

        LOGGER.error(String.valueOf(response));

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
