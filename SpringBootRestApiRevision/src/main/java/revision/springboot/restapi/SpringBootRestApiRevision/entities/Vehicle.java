
/*
 * This class is a template of a vehicle object.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Table(name = "vehicles")
@Entity
@SuppressWarnings("unused")
public class Vehicle {

    // Fields.
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long serialNumber;
    @Column(name = "vehicle_registration_number", unique = true, nullable = false)
    private String registrationNumber;

    @Column(nullable = false)
    private String brand;

    @Column(nullable = false)
    private String model;

    @Column(nullable = false)
    private String type;

    @Column(nullable = false)
    private String variant;

    @OneToMany(
            mappedBy = "vehicle",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            targetEntity = Owner.class
    )
    @JsonManagedReference
    private List<Owner> owners;

    @Version
    private long version;

    // Default Constructor.
    public Vehicle() {
        this.owners = new LinkedList<>();
    }

    // Parameterized Constructor.
    public Vehicle(String registrationNumber, String brand, String model, String type, String variant) {
        this.registrationNumber = registrationNumber;
        this.brand = brand;
        this.model = model;
        this.type = type;
        this.variant = variant;
    }

    // Getters and Setters for every field.
    public long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public List<Owner> getOwners() {
        return owners;
    }

    public void setOwners(List<Owner> owners) {
        this.owners = owners;
    }

    public long getVersion() { return version; }

    public void setVersion(long version) { this.version = version; }

    // ".equals()" method implementation to compare two "Vehicle" objects.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle vehicle)) return false;
        return getSerialNumber() == vehicle.getSerialNumber() && Objects.equals(getRegistrationNumber(), vehicle.getRegistrationNumber()) && Objects.equals(getBrand(), vehicle.getBrand()) && Objects.equals(getModel(), vehicle.getModel()) && Objects.equals(getType(), vehicle.getType()) && Objects.equals(getVariant(), vehicle.getVariant());
    }

    // "hashCode()" method implementation.
    @Override
    public int hashCode() {
        return Objects.hash(getSerialNumber(), getRegistrationNumber(), getBrand(), getModel(), getType(), getVariant(), getOwners());
    }
}
