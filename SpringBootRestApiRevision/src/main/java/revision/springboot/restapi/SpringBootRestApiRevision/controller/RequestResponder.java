
/*
 * This class object represents the "presentation layer (/ controller layer)".
 * This class object accepts all the valid URL requests and sends back response after processing the requests.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.Vehicle;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.VehicleServiceRecord;
import revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage.ServerResponse;
import revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage.ServerResponseWithDetails;
import revision.springboot.restapi.SpringBootRestApiRevision.service.ServiceProvider;

import java.util.List;

@RestController
@RequestMapping("/garage")
@SuppressWarnings("unused")
public class RequestResponder {

    @Autowired
    private ServiceProvider serviceProvider; // Getting the service layer object.

    private final Logger LOGGER = LoggerFactory.getLogger(RequestResponder.class);

    // Extracting all the vehicles from the garage (/ list of vehicles).
    @GetMapping("/vehicles")
    public ResponseEntity<List<Vehicle>> getAllVehicles() {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        return new ResponseEntity<>(this.serviceProvider.getAllVehicles(), HttpStatus.OK);
    }

    // Extracting a vehicle from the garage (/ list of vehicles) with the provided SERIAL NUMBER.
    @GetMapping("/vehicles/{serialNumber}")
    public ResponseEntity<Object> getVehicle(@PathVariable("serialNumber") long serialNumber) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        return new ResponseEntity<>(this.serviceProvider.getVehicle(serialNumber), HttpStatus.OK);
    }

    // Adding a new vehicle to the garage (/ list of vehicles).
    @PostMapping("/vehicles/add")
    public ResponseEntity<ServerResponseWithDetails> addVehicle(@RequestBody Vehicle vehicle
            , @RequestParam(name = "addDummyServiceRecords", required = false) boolean addDummyVehicleServiceRecords) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        return new ResponseEntity<>(this.serviceProvider.addVehicle(vehicle, addDummyVehicleServiceRecords), HttpStatus.CREATED);
    }

    // Updating the information of an existing vehicle in the garage (/ list of vehicles).
    @PutMapping("/vehicles/update")
    public ResponseEntity<ServerResponse> updateVehicle(@RequestBody Vehicle vehicle) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        return new ResponseEntity<>(this.serviceProvider.updateVehicle(vehicle), HttpStatus.ACCEPTED);
    }

    // Removing a vehicle from the garage (/ list of vehicles) with the provided SERIAL NUMBER.
    @DeleteMapping("/vehicles/{serialNumber}/delete")
    public ResponseEntity<ServerResponse> deleteVehicle(@PathVariable("serialNumber") long serialNumber) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        return new ResponseEntity<>(this.serviceProvider.deleteVehicle(serialNumber), HttpStatus.ACCEPTED);
    }

    // Adding dummy service records to the database and getting all the
    // records including the new service record(s) from the database.
    @PutMapping("/serviceRecords/dummies/{vRegNo}/{enable}")
    public ResponseEntity<List<VehicleServiceRecord>> getDummyServiceRecords(
            @PathVariable("vRegNo") String vehicleRegistrationNumber,
            @PathVariable("enable") boolean enableAddDummyVehiclesToServiceRecords) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        LOGGER.trace(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "()");
        return new ResponseEntity<>(
                this.serviceProvider.getDummyServiceRecords(vehicleRegistrationNumber,
                        enableAddDummyVehiclesToServiceRecords), HttpStatus.ACCEPTED);
    }
}
