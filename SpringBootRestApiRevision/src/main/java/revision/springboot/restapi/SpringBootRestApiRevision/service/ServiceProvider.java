
/*
 * This interface represents the "service layer".
 * The implementation of this interface provides all the business logics to process every valid request.
*/

package revision.springboot.restapi.SpringBootRestApiRevision.service;

import revision.springboot.restapi.SpringBootRestApiRevision.entities.Vehicle;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.VehicleServiceRecord;
import revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage.ServerResponse;
import revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage.ServerResponseWithDetails;

import java.util.List;

public interface ServiceProvider {

   // Extracting all the vehicles from the garage (/ list of vehicles).
   List<Vehicle> getAllVehicles();

   // Extracting a vehicle from the garage (/ list of vehicles) with the provided SERIAL NUMBER.
   Object getVehicle(long serialNumber);

   // Adding a new vehicle to the garage (/ list of vehicles).
   ServerResponseWithDetails addVehicle(Vehicle vehicle, boolean addDummyVehicleServiceRecords);

   // Updating the information of an existing vehicle in the garage (/ list of vehicles).
   ServerResponse updateVehicle(Vehicle vehicle);

   // Removing a vehicle from the garage (/ list of vehicles) with the provided SERIAL NUMBER.
   ServerResponse deleteVehicle(long serialNumber);

   // Adding dummy service records to the database and getting all the
   // records including the new service record(s) from the database.
   List<VehicleServiceRecord> getDummyServiceRecords(String vehicleRegistrationNumber,
                                                     boolean enableAddDummyVehiclesToServiceRecords);

}
