package revision.springboot.restapi.SpringBootRestApiRevision.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.Vehicle;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.VehicleServiceRecord;
import revision.springboot.restapi.SpringBootRestApiRevision.error.VehicleNotFoundException;
import revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage.ServerResponse;
import revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage.ServerResponseWithDetails;
import revision.springboot.restapi.SpringBootRestApiRevision.service.ServiceProvider;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RequestResponder.class)
class RequestResponderTest {

    @MockBean
    @SuppressWarnings("all")
    private ServiceProvider serviceProvider;

    @Autowired
    @SuppressWarnings("all")
    private MockMvc mockMvc;

    @Test
    void getAllVehicles() throws Exception {
        // Generating a list of sample vehicles.
        List<Vehicle> vehicles = getSampleVehicles(10);

        // Generating response from the service layer using the sample
        // list of vehicles along with the appropriate service method.
        when(serviceProvider.getAllVehicles()).thenReturn(vehicles);

        // Testing the URL "/garage/vehicles".
        for (int index = 0; index < vehicles.size(); index++) {
            mockMvc.perform(get("/garage/vehicles"))
                    .andExpect(status().isOk()).
                    andExpect(jsonPath("$.[" + index + "].serialNumber")
                            .value(1001 * (index + 1)))
                    .andExpect(jsonPath("$.[" + index + "].registrationNumber")
                            .value("REG INFO - " + (1001 * (index + 1))))
                    .andExpect(jsonPath("$.[" + index + "].brand")
                            .value("Honda"))
                    .andExpect(jsonPath("$.[" + index + "].model")
                            .value("Africa Twin"))
                    .andExpect(jsonPath("$.[" + index + "].type")
                            .value("2 Wheeler"))
                    .andExpect(jsonPath("$.[" + index + "].variant")
                            .value("Off Road"));
        }
    }

    @Test
    void getVehicle() throws Exception {
        // Creating a sample vehicle.
        Vehicle vehicle = getSampleVehicles(1).get(0);

        // Generating response from the service layer using the sample vehicle
        // along with the appropriate service method.
        when(serviceProvider.getVehicle(1001)).thenReturn(vehicle);

        // Testing the URL "/garage/vehicles/{serialNumber}" for existing vehicle.
        mockMvc.perform(get("/garage/vehicles/{serialNumber}", 1001))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.serialNumber").value("1001"))
                .andExpect(jsonPath("$.registrationNumber").value("REG INFO - 1001"))
                .andExpect(jsonPath("$.brand").value("Honda"))
                .andExpect(jsonPath("$.model").value("Africa Twin"))
                .andExpect(jsonPath("$.type").value("2 Wheeler"))
                .andExpect(jsonPath("$.variant").value("Off Road"));
    }

    @Test
    void addVehicle() throws Exception {
        // Creating the sample vehicle to be added.
        Vehicle vehicle = getSampleVehicles(1).get(0);

        // Assuming the response from the server.
        ServerResponse response = new ServerResponse("The vehicle has been added successfully.", vehicle);
        ServerResponseWithDetails responseWithDetails =
                new ServerResponseWithDetails(response, null);

        // Generating response from the service layer using the sample response
        // with details along with the appropriate service method.
        when(serviceProvider.addVehicle(vehicle, false))
                .thenReturn(responseWithDetails);

        // Testing the URL "/garage/vehicles/add" for existing vehicle.
        mockMvc.perform(post("/garage/vehicles/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(vehicle)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.response.message")
                        .value("The vehicle has been added successfully."))
                .andExpect(jsonPath("$.response.vehicle.serialNumber").value(1001))
                .andExpect(jsonPath("$.response.vehicle.registrationNumber").
                        value("REG INFO - 1001"))
                .andExpect(jsonPath("$.response.vehicle.brand").value("Honda"))
                .andExpect(jsonPath("$.response.vehicle.model").value("Africa Twin"))
                .andExpect(jsonPath("$.response.vehicle.type").value("2 Wheeler"))
                .andExpect(jsonPath("$.response.vehicle.variant").value("Off Road"));
    }

    @Test
    void updateVehicle() throws Exception {
        // Creating a sample vehicle as vehicle to be processed for update of data.
        Vehicle vehicle = new Vehicle();
        vehicle.setSerialNumber(9999);
        vehicle.setRegistrationNumber("OR-09-C-0878");
        vehicle.setBrand("TATA Motors");
        vehicle.setModel("Indica DLS V2");
        vehicle.setType("4 Wheeler");
        vehicle.setVariant("Hatchback");

        // Creating a sample vehicle as an updated vehicle.
        Vehicle updatedVehicle = new Vehicle();
        updatedVehicle.setSerialNumber(vehicle.getSerialNumber());
        updatedVehicle.setRegistrationNumber(vehicle.getRegistrationNumber());
        updatedVehicle.setBrand("Toyota");
        updatedVehicle.setModel("Corolla Altis");
        updatedVehicle.setType(vehicle.getType());
        updatedVehicle.setVariant("Sedan");

        // Generating response from the service layer using the sample vehicle
        // along with the appropriate service method.
        when(serviceProvider.updateVehicle(any(Vehicle.class))).thenReturn(
                new ServerResponse("The vehicle has been updated successfully.",
                        updatedVehicle));

        // Testing the URL "/garage/vehicles/update" for existing vehicle.
        mockMvc.perform(put("/garage/vehicles/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(vehicle)))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.vehicle.serialNumber").value(9999))
                .andExpect(jsonPath("$.vehicle.registrationNumber").value("OR-09-C-0878"))
                .andExpect(jsonPath("$.vehicle.brand").value("Toyota"))
                .andExpect(jsonPath("$.vehicle.model").value("Corolla Altis"))
                .andExpect(jsonPath("$.vehicle.type").value("4 Wheeler"))
                .andExpect(jsonPath("$.vehicle.variant").value("Sedan"))
                .andExpect(jsonPath("$.message")
                        .value("The vehicle has been updated successfully."));
    }

    @Test
    void deleteVehicle() throws Exception {
        // Generating response from the service layer using the sample vehicle
        // along with the appropriate service method.
        when(serviceProvider.deleteVehicle(1001)).thenReturn(
                new ServerResponse(
                        "The vehicle with serial number '1001' has been deleted successfully.", null));

        // Testing the URL "/garage/vehicles/{serialNumber}/delete" for existing vehicle.
        mockMvc.perform(delete("/garage/vehicles/{serialNumber}/delete", 1001))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.message")
                        .value("The vehicle with serial number '1001' has been deleted successfully."));
    }

    @Test
    void getDummyServiceRecords() throws Exception {
        // Creating a dummy list of vehicle service records as a response.
        List<VehicleServiceRecord> dummyServiceRecords = new LinkedList<>();
        for (int count = 1; count <= 9; count ++) {
            VehicleServiceRecord record = new VehicleServiceRecord(new Date(), "REG INFO" + (1001 * count));
            dummyServiceRecords.add(record);
        }

        // Generating response from the service layer using the sample vehicle
        // service records along with the appropriate service method.
        when(serviceProvider.getDummyServiceRecords("REG INFO", true))
                .thenReturn(dummyServiceRecords);

        // Testing the URL "/garage/serviceRecords/dummies/{vRegNo}/{enable}".
        mockMvc.perform(put("/garage/serviceRecords/dummies/{vRegNo}/{enable}", "REG INFO", true))
                .andExpect(status().isAccepted());
     }

     @Test
     void testingDifferentExceptions() throws Exception {

        // Testing the URL "/garage/vehicles/{serialNumber}" for non-existing vehicle.
         when(serviceProvider.getVehicle(1001))
                 .thenThrow(new VehicleNotFoundException(1001));

         mockMvc.perform(get("/garage/vehicles/{serialNumber}", 1001))
                         .andExpect(status().isNotFound())
                 .andExpect(result -> assertTrue(result.getResolvedException() instanceof VehicleNotFoundException))
                 .andExpect(result -> assertEquals(
                                         "Vehicle with serial number '1001' was not found!",
                                         Objects.requireNonNull(result.getResolvedException()).getMessage()));

         // Testing the URL "/garage/vehicles/update" for non-existing vehicle.
         when(serviceProvider.updateVehicle(any(Vehicle.class)))
                 .thenThrow(new VehicleNotFoundException(2002));

         mockMvc.perform(put("/garage/vehicles/update")
                 .contentType(MediaType.APPLICATION_JSON)
                 .content(new ObjectMapper().writeValueAsString(
                         getSampleVehicles(1).get(0))))
                 .andExpect(status().isNotFound())
                 .andExpect(result -> assertTrue(result.getResolvedException() instanceof VehicleNotFoundException))
                 .andExpect(result -> assertEquals(
                         "Vehicle with serial number '2002' was not found!",
                         Objects.requireNonNull(result.getResolvedException()).getMessage()));

         // Testing the URL "/garage/vehicles/{serialNumber}/delete" for non-existing vehicle.
         when(serviceProvider.deleteVehicle(3003))
                 .thenThrow(new VehicleNotFoundException(3003));

         mockMvc.perform(delete("/garage/vehicles/{serialNumber}/delete", 3003))
                 .andExpect(status().isNotFound())
                 .andExpect(result -> assertTrue(result.getResolvedException() instanceof VehicleNotFoundException))
                 .andExpect(result -> assertEquals(
                         "Vehicle with serial number '3003' was not found!",
                         Objects.requireNonNull(result.getResolvedException()).getMessage()));


         // Testing the URL "/garage/vehicles/add" for any exception other than
         // VehicleNotFoundException.
         when(serviceProvider.addVehicle(getSampleVehicles(1).get(0),false))
                 .thenThrow(new RuntimeException());

         mockMvc.perform(post("/garage/vehicles/add")
                         .contentType(MediaType.APPLICATION_JSON)
                         .content(new ObjectMapper().writeValueAsString(
                                 getSampleVehicles(1).get(0))))
                 .andExpect(status().isBadRequest());
     }

    // This function generates a sample list of vehicles with the specified number of vehicles.
    List<Vehicle> getSampleVehicles(int numberOfVehicles) {
        List<Vehicle> vehicleList = new LinkedList<>();
        for (int count = 1; count <= numberOfVehicles; count++) {
            Vehicle vehicle = new Vehicle();
            vehicle.setSerialNumber(1001L * count);
            vehicle.setRegistrationNumber("REG INFO - " + (1001 * count));
            vehicle.setBrand("Honda");
            vehicle.setModel("Africa Twin");
            vehicle.setType("2 Wheeler");
            vehicle.setVariant("Off Road");
            vehicleList.add(vehicle);
        }
        return vehicleList;
    }
}